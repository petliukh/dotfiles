local set = vim.opt

set.tabstop = 8
set.softtabstop = 8
set.expandtab = true
set.shiftwidth = 8
set.smarttab = true
