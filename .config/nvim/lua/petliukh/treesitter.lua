local treesitter = require("nvim-treesitter.configs")

treesitter.setup({
	ensure_installed = {
		"lua",
		"c",
		"cpp",
		"cmake",
		"python",
		"bash",
		"gitcommit",
	},
	modules = {},
	sync_install = false,
	auto_install = false,
	ignore_install = {},
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = false,
	},
	indent = {
		enable = false,
	},
})
