local formatter = require("formatter")

formatter.setup({
	filetype = {
		lua = {
			require("formatter.filetypes.lua").stylua,
		},

		python = {
			require("formatter.filetypes.python").black,
		},

		cpp = {
			require("formatter.filetypes.cpp").clangformat,
		},

		c = {
			require("formatter.filetypes.c").clangformat,
		},

		cmake = {
			require("formatter.filetypes.cmake").cmakeformat,
		},

		["*"] = {
			require("formatter.filetypes.any").remove_trailing_whitespace,
		},
	},
})
