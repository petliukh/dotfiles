local fzf = require("fzf-lua")
local spectre = require("spectre")
local harpoon_mark = require("harpoon.mark")
local harpoon_ui = require("harpoon.ui")
local hlsearch = false

-- Handy functions

local function nnoremap(lhs, rhs)
	vim.keymap.set("n", lhs, rhs, { noremap = true, silent = true })
end

local function vnoremap(lhs, rhs)
	vim.keymap.set("v", lhs, rhs, { noremap = true, silent = true })
end

local function inoremap(lhs, rhs)
	vim.keymap.set("i", lhs, rhs, { noremap = true, silent = true })
end

local function tnoremap(lhs, rhs)
	vim.keymap.set("t", lhs, rhs, { noremap = true, silent = true })
end

local function toggle_hlsearch()
	hlsearch = not hlsearch
	if hlsearch then
		vim.cmd("set hlsearch")
	else
		vim.cmd("set nohlsearch")
	end
end

-- Common
nnoremap("<c-h>", "5<c-w><")
nnoremap("<c-l>", "5<c-w>>")
nnoremap("<c-j>", "5<c-w>-")
nnoremap("<c-k>", "5<c-w>+")

nnoremap("<c-u>", "<c-u>zz")
nnoremap("<c-d>", "<c-d>zz")

vnoremap(">", ">gv")
vnoremap("<", "<gv")
vnoremap("J", ":m'>+1<cr>gv=gv")
vnoremap("K", ":m-2<cr>gv=gv")
inoremap("jj", "<Esc>")
inoremap("<c-j>", "<cmd>lua require('luasnip').jump(1)<cr>")
inoremap("<c-k>", "<cmd>lua require('luasnip').jump(-1)<cr>")
tnoremap("jj", "<c-\\><c-n>")

nnoremap("<leader>cp", [[<cmd>let @+ = expand("%:t") . ":" . line(".")<cr>]])
nnoremap("<leader>P", '"+p')
nnoremap("<s-ScrollWheelDown>", "4zl")
nnoremap("<s-ScrollWheelUp>", "4zh")
vnoremap("<leader>P", '"+P')
vnoremap("<leader>Y", '"+y')
nnoremap("<leader>H", toggle_hlsearch)
vnoremap("p", '"_dP')

-- Git
nnoremap("<leader>gg", ":Git<cr>:only<cr>")
nnoremap("<leader>gm", ":Git mergetool<cr>:only<cr>")
nnoremap("<leader>gds", ":Gvdiffsplit!<cr>")
nnoremap("<leader>gdh", ":diffget //2<cr>")
nnoremap("<leader>gdm", ":diffget //3<cr>")
nnoremap("<leader>gdb", "?<<<<<<<<cr>dd/=======<cr>dd/>>>>>>><cr>dd")
nnoremap("<leader>gdn", "?<<<<<<<<cr>V/>>>>>>><cr>d")

-- Netrw
nnoremap("<leader>.", [[:Explore <bar> :sil! /<c-R>=expand("%:t")<cr><cr>:noh<cr>]])
nnoremap("<leader><Tab>", ":Texplore<cr>")

-- LSP
nnoremap("[d", vim.diagnostic.goto_prev)
nnoremap("]d", vim.diagnostic.goto_next)
nnoremap("gd", vim.lsp.buf.definition)
nnoremap("gr", fzf.lsp_references)
nnoremap("L", vim.diagnostic.open_float)
nnoremap("K", vim.lsp.buf.hover)
nnoremap("<leader>D", fzf.diagnostics_document)
nnoremap("<leader>R", vim.lsp.buf.rename)
nnoremap("<leader>A", vim.lsp.buf.code_action)

-- Fzf
nnoremap("<leader>pf", fzf.files)
nnoremap("<leader>pg", fzf.live_grep)
nnoremap("<leader>gf", fzf.git_files)

nnoremap("<leader>fr", fzf.resume)
nnoremap("<leader>fh", fzf.helptags)
nnoremap("<leader>fq", fzf.quickfix)
nnoremap("<leader>fs", fzf.lsp_document_symbols)

-- Spectre
nnoremap("<leader>S", function()
	spectre.open()
	vim.cmd([[only]])
end)

-- Formatter
nnoremap("<leader>F", ":Format<cr>")

-- Harpoon
nnoremap("<leader>ha", harpoon_mark.add_file)
nnoremap("<leader>hh", harpoon_ui.toggle_quick_menu)
nnoremap("<leader>hn", harpoon_ui.nav_next)
nnoremap("<leader>hp", harpoon_ui.nav_prev)

-- Undotree
nnoremap("<leader>u", vim.cmd.UndotreeToggle)
