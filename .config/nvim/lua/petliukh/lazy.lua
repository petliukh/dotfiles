-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = "https://github.com/folke/lazy.nvim.git"
  local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	-- Base.
	"nvim-lua/plenary.nvim",

	-- LSP & completion.
	"williamboman/mason.nvim",
	"williamboman/mason-lspconfig.nvim",
	"WhoIsSethDaniel/mason-tool-installer.nvim",
	"neovim/nvim-lspconfig",

	"hrsh7th/cmp-nvim-lsp",
	"hrsh7th/cmp-buffer",
	"hrsh7th/cmp-path",
	"hrsh7th/cmp-cmdline",
	"hrsh7th/nvim-cmp",
	"L3MON4D3/LuaSnip",
	"saadparwaiz1/cmp_luasnip",

	-- Formatters.
	"mhartington/formatter.nvim",
	"michaeljsmith/vim-indent-object",

	-- Syntax highlighting.
	"nvim-treesitter/nvim-treesitter",
	"nvim-treesitter/playground",

	-- Navigation & searching.
	"ibhagwan/fzf-lua",
	"theprimeagen/harpoon",
	"windwp/nvim-spectre",
	"mbbill/undotree",

	-- Git integration.
	"tpope/vim-fugitive",
	"lewis6991/gitsigns.nvim",

	-- Appearence.
	"tjdevries/colorbuddy.nvim",
	"nvim-lualine/lualine.nvim",
	{
		"svrana/neosolarized.nvim",
		config = function()
			require("neosolarized").setup({
				comment_italics = true,
				background_set = true,
			})
			vim.cmd("colorscheme neosolarized")
		end,
	},
	{
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup()
		end,
	},

	-- Misc
	"numToStr/Comment.nvim",
}, {
	ui = {
		icons = {
			cmd = "⌘",
			config = "🛠",
			event = "📅",
			ft = "📂",
			init = "⚙",
			keys = "🗝",
			plugin = "🔌",
			runtime = "💻",
			require = "🌙",
			source = "📄",
			start = "🚀",
			task = "📌",
			lazy = "💤 ",
		},
	},
})
