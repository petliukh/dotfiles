. $HOME/.config/shell-agnostic/sys-env.sh
. $HOME/.config/shell-agnostic/usr-env.sh
. $HOME/.config/shell-agnostic/xdg-base-spec/env.sh
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export HISTSIZE=10000
export SAVEHIST=10000
export KEYTIMEOUT=1
