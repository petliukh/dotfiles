vi-append-wl-selection () {
    RBUFFER=$(wl-paste </dev/null)$RBUFFER;
}

zle -N vi-append-wl-selection
bindkey -a 'p' vi-append-wl-selection

vi-yank-wl-selection () {
    zle vi-yank
    print -rn -- $CUTBUFFER | wl-copy;
}

vi-delete-wl-selection () {
    zle vi-delete
    print -rn -- $CUTBUFFER | wl-copy;
}

zle -N vi-yank-wl-selection
zle -N vi-delete-wl-selection
bindkey -M vicmd 'y' vi-yank-wl-selection 
bindkey -M vicmd 'd' vi-delete-wl-selection 
