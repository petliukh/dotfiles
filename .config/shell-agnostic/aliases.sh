alias ls="ls --group-directories-first --color=auto"
alias la="LC_ALL=C ls -ah --group-directories-first --color=auto"
alias ll="LC_ALL=C ls -lah --group-directories-first --color=auto"

alias dotfs="git --git-dir=$HOME/Projects/dotfiles --work-tree=$HOME"
alias vdotfs="GIT_DIR=$HOME/Projects/dotfiles GIT_WORK_TREE=$HOME nvim"

alias v=nvim
alias xo=xdg-open
