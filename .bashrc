# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

. $HOME/.config/shell-agnostic/sys-env.sh
. $HOME/.config/shell-agnostic/usr-env.sh
. $HOME/.config/shell-agnostic/xdg-base-spec/env.sh
export HISTFILE=$XDG_STATE_HOME/bash/history

# User specific aliases and functions
. $HOME/.config/shell-agnostic/aliases.sh
. $HOME/.config/shell-agnostic/xdg-base-spec/aliases.sh
. $HOME/.config/shell-agnostic/scripts.sh
set -o vi
