#!/usr/bin/sh

for file in ./dotfiles/*; do
    if [ -f "$file" ]; then
        . "$file"
    fi
done

for file in ./xdg-base-spec/*; do
    if [ -f "$file" ]; then
        . "$file"
    fi
done
