#!/usr/bin/sh

if ! [ -d "$HOME/Projects/dotfiles" ]; then
    echo "[ERROR] Dotfiles directory does not exist at $HOME/Projects/dotfiles. Git clone the repo first"
    exit
fi

alias __dotfiles="git --git-dir="$HOME/Projects/dotfiles" --work-tree="$HOME""
core_worktree="$(__dotfiles config --local core.worktree)"

if [ -z "$core_worktree" ]; then
    __dotfiles config --local core.worktree "$HOME"
    echo "[SUCCESS] Set $HOME/Projects/dotfiles worktree to $HOME"
else
    echo "[SUCCESS] Dotfiles core worktree already set to $core_worktree"
fi

showuf="$(__dotfiles config --local status.showUntrackedFiles)"
if [ -z "$showuf" ] || [ "$showuf" != "no" ]; then
    __dotfiles config --local status.showUntrackedFiles no
    echo "[SUCCESS] Set status.showUntrackedFiles option to 'no' in dotfiles git dir config"
else
    echo "[SUCCESS] Dotfiles untracked files are already hidden(status.showUntrackedFiles=$showuf)"
fi
