#!/usr/bin/sh

if [ -z "$XDG_CONFIG_HOME" ]; then
    echo "[ERROR] XDG_CONFIG_HOME not set, PLEASE FIX"
    exit
fi

mkdir -p "$XDG_CONFIG_HOME"/git

if [ -f "$HOME"/.gitconfig ]; then
    mv "$HOME"/.gitconfig "$XDG_CONFIG_HOME"/git/config
    echo "[SUCCESS] moved $HOME/.gitconfig to $XDG_CONFIG_HOME/git/config"
else
    if [ -f "$XDG_CONFIG_HOME"/git/config ]; then
        echo "[SUCCESS] git config is already at the correct location($XDG_CONFIG_HOME/git/config)"
    else
        touch "$XDG_CONFIG_HOME"/git/config
        echo "[SUCCESS] created git config file at $XDG_CONFIG_HOME/git/config"
    fi
fi
