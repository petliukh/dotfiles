#!/usr/bin/sh

if [ -z "$XDG_CONFIG_HOME" ]; then
    echo "[ERROR] XDG_CONFIG_HOME not set, PLEASE FIX"
    exit
fi

if [ -f "$HOME"/.nvidia-settings-rc ]; then
    if [ -f "$XDG_CONFIG_HOME"/nvidia/settings ]; then
        rm "$HOME"/.nvidia-settings-rc
        echo "[SUCCESS] nvidia-settings-rc is already at the correct location ($XDG_CONFIG_HOME/nvidia/settings), removed unnecessary $HOME/.nvidia-settings-rc"
    else
        mkdir -p "$XDG_CONFIG_HOME"/nvidia
        mv "$HOME"/.nvidia-settings-rc "$XDG_CONFIG_HOME"/nvidia/settings
        echo "[SUCCESS] Moved $HOME/.nvidia-settings-rc to $XDG_CONFIG_HOME/nvidia/settings"
    fi
else
    if [ -f "$XDG_CONFIG_HOME"/nvidia/settings ]; then
        echo "[SUCCESS] nvidia settings config file is already at the correct location("$XDG_CONFIG_HOME"/nvidia/settings)"
    else
        mkdir -p "$XDG_CONFIG_HOME"/nvidia
        echo "[SUCCESS] Created nvidia "$XDG_CONFIG_HOME"/nvidia config dir"
    fi
fi

echo "[WARNING] Please make sure that nvidia-settings alias is defined to use custom --config $XDG_CONFIG_HOME/nvidia/settings"
