#!/usr/bin/sh

if [ -z "$XDG_CACHE_HOME" ]; then
    echo "[ERROR] XDG_CACHE_HOME not set, PLS FIX"
    exit
fi

if [ -z "$CUDA_CACHE_PATH" ]; then
    echo "[ERROR] CUDA_CACHE_PATH env variable not set, PLS FIX"
    exit
fi

if [ "$CUDA_CACHE_PATH" != "$XDG_CACHE_HOME/nv" ]; then
    echo "[ERROR] CUDA_CACHE_PATH=$CUDA_CACHE_PATH env variable is not set to XDG base spec compliant dir, PLS FIX"
    exit
fi

if [ -d "$HOME/.nv" ]; then
    if [ -d "$XDG_CACHE_HOME/nv" ]; then
        rm -r "$HOME/.nv"
        echo "[SUCCESS] CUDA cache dir is already at the correct location, removed unnecesary $HOME/.nv dir"
    else
        mv "$HOME/.nv" "$XDG_CACHE_HOME/nv"
        echo "[SUCCESS] Moved CUDA cache dir from $HOME/.nv to $XDG_CACHE_HOME/nv"
    fi
else
    if [ -d "$XDG_CACHE_HOME/nv" ]; then
        echo "[SUCCESS] CUDA cache dir is already at the correct location"
    else
        mkdir -p "$XDG_CACHE_HOME/nv"
        echo "[SUCCESS] Created CUDA cache dir at $XDG_CACHE_HOME/nv"
    fi
fi
