#!/usr/bin/bash

if [ -z "$XDG_CONFIG_HOME" ]; then
    echo "[ERROR] XDG_CONFIG_HOME not set, PLEASE FIX"
    exit
fi

if [ -z "$XDG_CACHE_HOME" ]; then
    echo "[ERROR] XDG_CACHE_HOME not set, PLEASE FIX"
    exit
fi

if [ -z "$XDG_RUNTIME_DIR" ]; then
    echo "[ERROR] XDG_RUNTIME_DIR not set, PLEASE FIX"
    exit
fi

if [ -z "$NPM_CONFIG_INIT_MODULE" ]; then
    echo "[ERROR] NPM_CONFIG_INIT_MODULE not set, PLEASE FIX"
    exit
fi

if [ -z "$NPM_CONFIG_CACHE" ]; then
    echo "[ERROR] NPM_CONFIG_CACHE not set, PLEASE FIX"
    exit
fi

if [ -z "$NPM_CONFIG_TMP" ]; then
    echo "[ERROR] NPM_CONFIG_TMP not set, PLEASE FIX"
    exit
fi

if [ -d "$HOME/.npm" ]; then
    if [ -d "$NPM_CONFIG_CACHE" ]; then
        rm -r "$HOME/.npm"
        echo "[SUCCESS] npm cache is already at the correct location($NPM_CONFIG_CACHE), removed unnecessary $HOME/.npm dir"
    else
        mv "$HOME/.npm" "$NPM_CONFIG_CACHE"
        echo "[SUCCESS] moved $HOME/.npm cache dir to $NPM_CONFIG_CACHE"
    fi
else
    if [ -d "$NPM_CONFIG_CACHE" ]; then
        echo "[SUCCESS] npm cache is already at the correct location($NPM_CONFIG_CACHE)"
    else
        mkdir -p "$NPM_CONFIG_CACHE"
        echo "[SUCCESS] created npm cache dir at $NPM_CONFIG_CACHE"
    fi
fi
