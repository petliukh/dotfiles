#!/usr/bin/sh

if [ -z "$XDG_DATA_HOME" ]; then
    echo "[ERROR] XDG_DATA_HOME not set, PLEASE FIX"
    exit
fi

if [ -z "$CARGO_HOME" ]; then
    echo "[ERROR] CARGO_HOME not set, PLEASE FIX"
    exit
fi

if [ -d "$HOME/.cargo" ]; then
    if [ -d "$CARGO_HOME" ]; then
        rm -r "$HOME/.cargo"
        echo "[SUCCESS] cargo data is already at the correct location, rmed unnecessary $HOME/.cargo dir"
    else
        mv "$HOME/.cargo" "$CARGO_HOME"
        echo "[SUCCESS] Moved $HOME/.cargo to $CARGO_HOME"
    fi
else
    if [ -d "$CARGO_HOME" ]; then
        echo "[SUCCESS] cargo data is already at the correct location"
    else
        echo "[WARNING] cargo data dir does not exist at any of the usual locations, please check if cargo is installed"
    fi
fi
