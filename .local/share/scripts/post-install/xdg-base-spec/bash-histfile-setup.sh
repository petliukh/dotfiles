#!/usr/bin/sh

if [ -z "$XDG_STATE_HOME" ]; then
    echo "[ERROR] XDG_STATE_HOME env variable not set, PLEASE FIX"
    exit
fi

bash_histfile="$HISTFILE"

if [ -z "$bash_histfile" ]; then
    echo "[ERROR] Bash HISTFILE not set, PLEASE FIX"
    exit
fi

if [ "$bash_histfile" = "$XDG_STATE_HOME/bash/history" ]; then
    echo "[INFO] HISTFILE set correctly, checking if the actual file exists..."
    mkdir -p "$XDG_STATE_HOME/bash"

    if [ -f "$HOME/.bash_history" ]; then
        mv "$HOME/.bash_history" "$XDG_STATE_HOME/bash/history"
        echo "[SUCCESS] Moved $HOME/.bash_history to $XDG_STATE_HOME/bash/history"
    elif ! [ -f "$XDG_STATE_HOME/bash/history" ]; then
        touch "$XDG_STATE_HOME/bash/history"
        echo "[SUCCESS] Created XDG compliant bash history file at $XDG_STATE_HOME/bash/history"
    else
        echo "[SUCCESS] HISTFILE already exists at $XDG_STATE_HOME/bash/history"
    fi
else
    echo "[ERROR] Bash HISTFILE=$bash_histfile not set to XDG_BASE compliant directory, PLEASE FIX"
fi
